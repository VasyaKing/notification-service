package com.ebatkontora.notificationservice;

import com.ebatkontora.notificationservice.dto.Notification;
import com.ebatkontora.notificationservice.dto.Tag;
import com.ebatkontora.notificationservice.service.AmoService;
import com.ebatkontora.notificationservice.store.LocalStore;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.Collections;
import java.util.List;

//@SpringBootTest
public class AmoServiceTest {

    String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjBiNmZlNzM5ZDFlNDE4MzY4ZTg2ZWQxM2MyZDg1NTVjNDUzY2EzNmY3YTIxNDdiMTVlZDE0ZjczYjYzYzRlNzcwOWNhMTg2NmI2Y2QzN2Y0In0.eyJhdWQiOiI1MmNiMDJjMi1hZWIyLTQ3ZTMtYmViZi0wMzFiZDdjYTYwYzciLCJqdGkiOiIwYjZmZTczOWQxZTQxODM2OGU4NmVkMTNjMmQ4NTU1YzQ1M2NhMzZmN2EyMTQ3YjE1ZWQxNGY3M2I2M2M0ZTc3MDljYTE4NjZiNmNkMzdmNCIsImlhdCI6MTY0MjE4NzQ5MCwibmJmIjoxNjQyMTg3NDkwLCJleHAiOjE2NDIyNzM4OTAsInN1YiI6IjU4OTk4ODUiLCJhY2NvdW50X2lkIjoyODgyNzk4OCwic2NvcGVzIjpbInB1c2hfbm90aWZpY2F0aW9ucyIsImNybSIsIm5vdGlmaWNhdGlvbnMiXX0.dWayyIivBOrqbLujYxriGXeo0SsouWYVljwgrHCqURryiZ2W59MTcYsEno0gb1bzWW1pOEWpWi1lx-Y2xsTUuO0u8Cf-PWhZHfKCB3gyIMNHM8NClWywdr1osZb2MuCuxjTLIfelVKDf02MQYJU6xPGUVcDqKFHn0hOMJ03S-Poz_tiuGdxZzJVXaSkl1mnVldwKq_0vXZHk58zxDAha_GJ25PkBB-HP_8PHnmSFKYQxlGmrnznNkTh4V9729MfO1gkDP0lbh0pxRZDo1LuHftVBWho1S4sbmSMNGxAm8xlwQpYFf_RDiL3SbasagpcS8VWqYItsnnVN0RMeFPG-Nw";

    @Autowired
    private AmoService amoService;

    @Autowired
    private LocalStore localStore;

//    @Test
    public void filterBuyTest() {
        localStore.getCredentials().put("accessToken", token);
        amoService.initAmoHeaders();

        Tag tag1 = new Tag();
        Tag tag2 = new Tag();
        Tag tag3 = new Tag();
        tag1.setName("Volkovskaya");
        tag2.setName("KRESTY");
        tag3.setName("Sobstvennik");

        Notification notification = Notification.builder()
                .id("1563993")
                .geoMainText("ТЕСТ")
                .formattedPrice("11 млн ₽")
                .roomsCountText("2-комн.")
                .areaText("45 м²")
                .tags(Collections.singletonList(tag3))
                .undergrounds(List.of(tag1, tag2))
                .dealTypeText("Продать")
                .offerTypeText("Жилая")
                .comment("TEST COMMENT")
                .customerName("TEST NAME")
                .customerTelephone("9214442226")
                .build();

       amoService.createLead(notification);
    }
}
