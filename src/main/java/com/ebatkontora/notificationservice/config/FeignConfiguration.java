package com.ebatkontora.notificationservice.config;

import com.ebatkontora.notificationservice.store.LocalStore;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.StdConverter;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import feign.Client;
import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;

@Configuration
public class FeignConfiguration {

    private final LocalStore localStore;
    private final String host;
    private final int port;
    private final String username;
    private final String password;

    @Autowired
    public FeignConfiguration(LocalStore localStore) {
        this.localStore = localStore;
        port = Integer.parseInt(localStore.getProxy().get("port"));
        host = localStore.getProxy().get("host");
        username = localStore.getProxy().get("user");
        password = localStore.getProxy().get("password");
    }

    @Bean
    public OkHttpClient feignClient() { //todo seems it doesn't work
        Authenticator proxyAuthenticator = (route, response) -> {
            String credential = Credentials.basic(username, password);
            return response.request().newBuilder()
                    .header("Proxy-Authorization", credential)
                    .build();
        };

        return new OkHttpClient.Builder()
//                .connectTimeout(60, TimeUnit.SECONDS)
//                .writeTimeout(60, TimeUnit.SECONDS)
//                .readTimeout(60, TimeUnit.SECONDS)
                .proxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(host, port)))
                .proxyAuthenticator(proxyAuthenticator)
                .build();
    }

    @Bean
    public RestTemplate restTemplate() {
        HttpComponentsClientHttpRequestFactory factory = null;
        var isProxyEnabled = localStore.getProxy().get("isProxyEnabled").equals("true");
        if (isProxyEnabled) {
            HttpHost myProxy = new HttpHost(host, port);
            CredentialsProvider credsProvider = new BasicCredentialsProvider();
            credsProvider.setCredentials(new AuthScope(host, port), new UsernamePasswordCredentials(username, password));

            HttpClient httpClient = HttpClients.custom()
                    .setDefaultCredentialsProvider(credsProvider)
                    .setProxy(myProxy)
                    .disableCookieManagement()
                    .build();

            factory = new HttpComponentsClientHttpRequestFactory();
            factory.setHttpClient(httpClient);
        }

        RestTemplate restTemplate = Objects.nonNull(factory) ? new RestTemplate(factory) : new RestTemplate();
        MappingJackson2HttpMessageConverter jsonHttpMessageConverter = new MappingJackson2HttpMessageConverter();
        jsonHttpMessageConverter.getObjectMapper().configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        restTemplate.getMessageConverters().add(jsonHttpMessageConverter);

        return restTemplate;
    }

    public static class LongToLocalDateTimeConverter extends StdConverter<Long, LocalDateTime> {
        public LocalDateTime convert(final Long value) {
            return Instant.ofEpochSecond(value).atZone(ZoneId.systemDefault()).toLocalDateTime();
        }
    }

}
