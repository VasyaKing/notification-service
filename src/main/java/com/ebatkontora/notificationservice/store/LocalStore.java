package com.ebatkontora.notificationservice.store;

import com.ebatkontora.notificationservice.dto.Notification;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
@Getter
@Slf4j
public class LocalStore {//todo move all store in redis
    private Map<String, Notification> notifications = new HashMap<>();
    private final Map<String, Boolean> chatIdsForCian = new HashMap<>();
    private final Map<String, String> credentials = new HashMap<>();
    private final Map<ActionCondition, Boolean> conditions = new ConcurrentHashMap<>();
    private final Map<String, String> proxy = new HashMap<>();

    @PostConstruct
    public void load() throws IOException {
        var cianLines = Files.readAllLines(Paths.get("../crds/cian-token.txt"));
        credentials.put("cianCookie", cianLines.get(0));
        credentials.put("cianCookieRefund", cianLines.get(1));
        credentials.put("cianCookieMy", cianLines.get(2));
        log.info("Cian token loaded from file");

        var proxyLines = Files.readAllLines(Paths.get("../crds/proxy.txt"));
        proxy.put("isProxyEnabled", proxyLines.get(0));
        proxy.put("host", proxyLines.get(1));
        proxy.put("port", proxyLines.get(2));
        proxy.put("user", proxyLines.get(3));
        proxy.put("password", proxyLines.get(4));
        log.info("Proxy settings loaded from file");
    }

    public LocalStore() {
        conditions.put(ActionCondition.BUY, false);
        conditions.put(ActionCondition.SELL, false);
        chatIdsForCian.put("101006488", true); //me
//        chatIdsForCian.put("384674648", true); //aziz

    }

    public void resetNotificationStore() {
        notifications = new HashMap<>();
    }
}
