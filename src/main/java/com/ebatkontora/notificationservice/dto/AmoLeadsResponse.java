package com.ebatkontora.notificationservice.dto;

import com.ebatkontora.notificationservice.config.FeignConfiguration;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AmoLeadsResponse
{
    @JsonProperty("_embedded")
    private Embedded embedded;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Embedded {
        private List<Lead> leads;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Lead {
        private String id;
        private String name;
        @JsonProperty("responsible_user_id")
        private String responsibleUserId;
        @JsonProperty("status_id")
        private int statusId;
        @JsonProperty("pipeline_id")
        private String pipelineId;
        @JsonProperty("custom_fields_values")
        private List<CustomField> customFieldsValues;
        @JsonProperty("_embedded")
        private NestedWrapper nestedWrapper;
        @JsonProperty("created_at")
        @JsonDeserialize(converter = FeignConfiguration.LongToLocalDateTimeConverter.class)
        private LocalDateTime createDate;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CustomField {
        @JsonProperty("field_id")
        private int id;
        private List<Value> values;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Value {
        @JsonProperty("enum_id")
        private String id;
        private String value;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class NestedWrapper {
        private List<Tag> tags;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Tag
    {
        private int id;
        private String name;
    }

}
