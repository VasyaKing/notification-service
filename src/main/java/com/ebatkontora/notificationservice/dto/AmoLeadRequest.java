package com.ebatkontora.notificationservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AmoLeadRequest {

    private String name;

    private int price;

    @JsonProperty("pipeline_id")
    private int pipelineId;

    @JsonProperty("custom_fields_values")
    @Builder.Default
    private List<CustomField> customFieldsValues = new ArrayList<>();

    @JsonProperty("_embedded")
    private Embedded embedded;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class CustomField {
        @JsonProperty("field_id")
        private int fieldId;
        @Singular
        private List<Value> values;
    }

    @Data
    @AllArgsConstructor
    public static class Value { //todo refactor , change to general model
        private String value;
    }


    @Data
    @AllArgsConstructor
    public static class Embedded {
        private List<Tag> tags;
    }


    @Data
    @AllArgsConstructor
    public static class Tag {
        private int id;
    }
}
