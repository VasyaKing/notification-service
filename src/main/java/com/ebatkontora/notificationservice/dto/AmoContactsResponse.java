package com.ebatkontora.notificationservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AmoContactsResponse {
    @JsonProperty("_embedded")
    private Embedded embedded;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Embedded {
        private List<Wrapper> contacts;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Wrapper {
        private int id;
    }
}
