package com.ebatkontora.notificationservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Singular;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AmoContactsRequest {
    private String name;

    @JsonProperty("custom_fields_values")
    @Builder.Default
    private List<CustomField> customFieldsValues = new ArrayList<>();

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    public static class CustomField {
        @JsonProperty("field_id")
        private int fieldId;
        @Singular
        private List<Value> values;
    }

    @Data
    @AllArgsConstructor
    public static class Value {
        private String value;
        @JsonProperty("enum_id")
        private Integer enumId;

        public Value(String value) {
            this.value = value;
        }
    }

}
