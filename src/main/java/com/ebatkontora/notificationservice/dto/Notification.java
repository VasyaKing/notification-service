package com.ebatkontora.notificationservice.dto;

import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode
public class Notification {
     private String id;
     private String dealTypeText;
     private String offerTypeText;
     private String viewsCount;
     private String startDate;
     private String endDate;
     private String urgently;
     private String formattedPrice;
     private Boolean isViewed;
     private String objectTypeText;
     private String areaText;
     private List<String> geoText;
     private String geoMainText;
     private List<Tag> undergrounds;
     private String roomsCountText;
     private List<Tag> tags;
     private boolean isBought;
     private String customerName;
     private String customerTelephone;
     private String comment;
     private String params;
     private String errorMessage;
}
