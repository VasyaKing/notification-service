package com.ebatkontora.notificationservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AmoLeadPatchRequest
{
    private int statusId;

    @JsonProperty("status_id")
    public int getStatusId() {
        return statusId;
    }
}
