package com.ebatkontora.notificationservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AmoPipelinesResponse
{
    @JsonProperty("_embedded")
    private EmbeddedPipeline embeddedPipeline;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class EmbeddedPipeline
    {
        private List<Pipeline> pipelines;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class EmbeddedStatus
    {
        private List<Status> statuses;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @EqualsAndHashCode
    public static class Pipeline {
        private String id;
        private String name;
        @JsonProperty("_embedded")
        private EmbeddedStatus embeddedStatus;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @EqualsAndHashCode
    public static class Status {
        private String id;
        private String name;
    }

}
