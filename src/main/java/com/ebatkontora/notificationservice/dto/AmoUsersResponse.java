package com.ebatkontora.notificationservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AmoUsersResponse {
    @JsonProperty("_embedded")
    private Embedded embedded;

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Embedded { //todo can I do this without embedded class ?
        private List<User> users;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @EqualsAndHashCode
    public static class User {
        private String id;
        private String name;
    }

}
