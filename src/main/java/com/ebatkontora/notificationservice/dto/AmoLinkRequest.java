package com.ebatkontora.notificationservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AmoLinkRequest {

    @JsonProperty("to_entity_id")
    private int toEntityId;

    @JsonProperty("to_entity_type")
    private String toEntityType;

    @Builder.Default
    private Metadata metadata = new Metadata(true);

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Metadata {
        @JsonProperty("is_main")
        private boolean isMain;
    }
}
