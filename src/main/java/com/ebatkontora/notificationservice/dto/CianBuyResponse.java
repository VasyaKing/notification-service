package com.ebatkontora.notificationservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CianBuyResponse {

    private MessageData messageData;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class MessageData {
        private String fullName;
        private String comment;
        private String params;
        private Phone phone;

        public String getPhoneNumber() {
            return phone.getCountryCode() + phone.getPhoneNumber();
        }
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Phone {
        private String countryCode;
        private String phoneNumber;
    }
}
