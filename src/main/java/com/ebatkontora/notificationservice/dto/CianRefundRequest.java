package com.ebatkontora.notificationservice.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class CianRefundRequest {
    private final String phone = "79697081000";
    private final String email = "dzhurakulov178@yandex.ru";
    private final String subject = "money";
    private final String userName = "Марк";
    private String text;

    public CianRefundRequest(String id, String cause)
    {
        this.text = String.format("Добрый день, прошу вернуть стоимость заявки %s - %s", id, cause);
    }
}