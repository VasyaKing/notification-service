package com.ebatkontora.notificationservice.service;

import com.ebatkontora.notificationservice.dto.CianRefundRequest;
import com.ebatkontora.notificationservice.dto.CianRefundResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "cianClient", url = "https://my.cian.ru")
public interface CianFeignClient
{
    @PostMapping("ud-api.cian.ru/feedback-form/v1/send-feedback/") //todo use it instead of restTemplate
    CianRefundResponse refund(@RequestHeader HttpHeaders httpHeaders,
                              @RequestBody CianRefundRequest cianRefundRequest);

    @GetMapping("leads/{id}")
    String getNotificationById(@PathVariable String id, @RequestHeader(value = "Cookie") String cookie);

}