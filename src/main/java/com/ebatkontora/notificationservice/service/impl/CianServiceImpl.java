package com.ebatkontora.notificationservice.service.impl;

import com.ebatkontora.notificationservice.dto.CianBuyResponse;
import com.ebatkontora.notificationservice.dto.CianRefundRequest;
import com.ebatkontora.notificationservice.dto.CianRefundResponse;
import com.ebatkontora.notificationservice.dto.Notification;
import com.ebatkontora.notificationservice.dto.Tag;
import com.ebatkontora.notificationservice.service.CianFeignClient;
import com.ebatkontora.notificationservice.service.CianService;
import com.ebatkontora.notificationservice.store.ActionCondition;
import com.ebatkontora.notificationservice.store.LocalStore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.UnknownContentTypeException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CianServiceImpl implements CianService {

    private final static String GEO_TEXT_SPB = "Санкт-Петербург";
//    private final static String GEO_TEXT_PARGOLOVO = "Парголово";
    private final static String GEO_TEXT_KUDROVO = "Кудрово";
    private final static String GEO_TEXT_MURINO = "Мурино";
    private final static String TAG_AGENT = "Агент";
    private final static String TAG_HAS_AD = "Есть объявление";
    private final static String OFFER_TYPE_TEXT = "Жилая";
    private final static String PRICE_EXTENT = "млн";
    private final static int MINIMAL_PRICE = 7;
    private final static Pattern PATTERN_PRICE_COMMA = Pattern.compile("[0-9]+");
    private final static Pattern PATTERN_PRICE_WITHOUT_COMMA = Pattern.compile("(\\d+)(?!.*\\d)");

    private final RestTemplate restTemplate;
    private final LocalStore localStore;
    private final ObjectMapper objectMapper;
    private final CianFeignClient cianFeignClient; //todo use feign with headers
    private final String getMessagesUrl;
    private final String buyMessageUrl;
    private final Logger log = LogManager.getLogger();

    private HttpEntity<HashMap<String, Object>> getRequest;
    private HttpEntity<HashMap<String, Object>> buyRequest;
    private HttpHeaders refundHeaders = new HttpHeaders();

    public CianServiceImpl(RestTemplate restTemplate, ObjectMapper objectMapper, CianFeignClient cianFeignClient,
                           LocalStore localStore, @Value("${cian.url}") String baseUrl) {
        this.restTemplate = restTemplate;
        this.localStore = localStore;
        this.objectMapper = objectMapper;
        this.cianFeignClient = cianFeignClient;
        this.getMessagesUrl = baseUrl + "get-messages/";
        this.buyMessageUrl = baseUrl + "buy-message/";

        init();
    }

    @Override
    public List<Notification> pollNotifications() throws UnknownContentTypeException {
        List<Notification> messages = new ArrayList<>();

        if (Objects.isNull(getRequest)) {
            log.error("Request is not initialized");
            return messages;
        }
        var response = restTemplate.postForObject(getMessagesUrl, getRequest, LinkedHashMap.class);
        TypeReference<List<Notification>> typeRefResp = new TypeReference<>() {
        };
        var messagesObj = Objects.isNull(response) ? null : response.get("messages");
        Optional<Object> respOptional = Optional.ofNullable(messagesObj);

        respOptional.ifPresentOrElse(m -> messages.addAll(objectMapper.convertValue((m), typeRefResp)),
                () -> log.error("Empty messages object in response"));

        return messages;
    }

    @Override
    public void filterAndBuy(Notification n) {
        log.info("Filter and buy notification: {}", n.getFormattedPrice());
        boolean priceCondition = false;
        String price = n.getFormattedPrice();
        boolean isSellEnabled = localStore.getConditions().get(ActionCondition.SELL);
        boolean isBuyEnabled = localStore.getConditions().get(ActionCondition.BUY);

        log.info("isSellEnabled: {}, isBuyEnabled: {}", isSellEnabled, isBuyEnabled);

        if (n.getDealTypeText().equals("Продать") && !isSellEnabled) {
            return;
        }

        if (n.getDealTypeText().toLowerCase().contains("купить") && !isBuyEnabled) {
            return;
        }

        if (Objects.nonNull(price) && price.contains(PRICE_EXTENT)) {
            int commaIndex = price.indexOf(",");
            if (commaIndex > 0) {
                log.info("Filter with comma in price");
                Matcher matcher = PATTERN_PRICE_COMMA.matcher(price.substring(0, commaIndex));
                if (matcher.find()) {
                    String priceMln = matcher.group(0);
                    int priceNumber = Integer.parseInt(priceMln);
                    priceCondition = priceNumber >= MINIMAL_PRICE;
                }
            } else {
                log.info("Filter without comma in price");
                Matcher matcher = PATTERN_PRICE_WITHOUT_COMMA.matcher(price);
                if (matcher.find()) {
                    String priceMln = matcher.group(0);
                    int priceNumber = Integer.parseInt(priceMln);
                    priceCondition = priceNumber >= MINIMAL_PRICE;
                }
            }
        }

        Optional<Tag> stopTag = n.getTags().stream()
                .filter(t -> TAG_AGENT.equals(t.getName()) || TAG_HAS_AD.equals(t.getName()))
                .findFirst();

        //"Жилая  / теги НЕ Агент или НЕ есть объявление / + города
        boolean generalCondition = stopTag.isEmpty()
                && (Objects.nonNull(n.getGeoMainText()) && (n.getGeoMainText().contains(GEO_TEXT_SPB)
                || n.getGeoMainText().contains(GEO_TEXT_MURINO)
//                || n.getGeoMainText().contains(GEO_TEXT_PARGOLOVO)
                || n.getGeoMainText().contains(GEO_TEXT_KUDROVO)))
                && OFFER_TYPE_TEXT.equals(n.getOfferTypeText());

        boolean priceIsNull = Objects.isNull(n.getFormattedPrice()) || n.getFormattedPrice().isEmpty();

        if ((generalCondition && priceCondition) || (generalCondition && priceIsNull)) {
//            buyNotification(n);
        }
    }


    @Override
    public void init() {
        log.info("Init cian requests");
        HttpHeaders headers = new HttpHeaders();
        headers.set("Cookie", localStore.getCredentials().get("cianCookie"));

        TypeReference<HashMap<String, Object>> typeRef = new TypeReference<>() {
        };
        try {
            var getRequestBody = objectMapper.readValue(
                    this.getClass().getResourceAsStream("/cian/get-request-body.json"), typeRef);
            var getRequestMyNotificationsBody = new HashMap<>(getRequestBody);
            getRequestMyNotificationsBody.put("messageState", "inProgress");
            var buyRequestBody = objectMapper.readValue(
                    this.getClass().getResourceAsStream("/cian/buy-request-body.json"), typeRef);
            getRequest = new HttpEntity<>(getRequestBody, headers);
            buyRequest = new HttpEntity<>(buyRequestBody, headers);


            //todo remove unnecessary headers
            refundHeaders.set("Cookie", localStore.getCredentials().get("cianCookieRefund"));
            refundHeaders.set("Host", "api.cian.ru");
            refundHeaders.set("Connection", "keep-alive");
            refundHeaders.set("Origin", "https://www.cian.ru");
            refundHeaders.set("Referer", "https://www.cian.ru/");
            refundHeaders.set("Accept", "*/*");
            refundHeaders.set("Sec-Fetch-Site", "same-site");
            refundHeaders.set("Sec-Fetch-Mode", "cors");
            refundHeaders.set("Sec-Fetch-Dest", "empty");
            refundHeaders.set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0");
            refundHeaders.set("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");

            log.info("Cian requests created");
        } catch (IOException e) {
            log.error("Can't init cian requests, cause: {}", e.getMessage());
        }
    }

    //todo remove duplications
    @Override
    public CianBuyResponse.MessageData getMessageDataByNotificationById(String id) throws JsonProcessingException {
        String html = cianFeignClient.getNotificationById(id, localStore.getCredentials().get("cianCookieMy"));
        Document parse = Jsoup.parse(html);
        String data = parse.childNode(1).childNode(0).childNode(69).childNode(0).attributes().get("#data");
        String temp = data.substring(data.indexOf("\"messageData\":") + 14, data.indexOf(",\"messageStatus\""));
        return objectMapper.readValue(temp, CianBuyResponse.MessageData.class);
    }

    @Override
    public void buyNotification(Notification notification) {
        Optional.ofNullable(buyRequest.getBody()).ifPresentOrElse(
                body -> {
                    log.info("Attempt to buy. Notification id = {}", notification.getId());
                    body.put("messageId", Integer.parseInt(notification.getId()));
                    try {
                        var linkedHashMap = restTemplate.postForObject(buyMessageUrl, buyRequest, LinkedHashMap.class);
                        log.info("CIAN linkedHashMap: {}", linkedHashMap);
                        var response = objectMapper.convertValue(linkedHashMap, CianBuyResponse.class);
                        notification.setBought(true);
                        if (response == null || response.getMessageData() == null) {
                            log.error("Response from CIAN is null or messageData is null");
                        } else {
                            notification.setCustomerName(response.getMessageData().getFullName());
                            notification.setCustomerTelephone(response.getMessageData().getPhoneNumber());
                            notification.setComment(response.getMessageData().getComment());
                            notification.setParams(response.getMessageData().getParams());
                        }

                        log.info("Successful purchase. Notification = {}", notification.toString());
                    } catch (HttpClientErrorException e) {
                        log.error(e.getMessage());
                        notification.setErrorMessage(e.getMessage());
                    }
                },
                () -> log.error("BuyRequest without body")
        );
    }

    public String sendRefundLetter(String id, String cause) {
        log.info("CIAN send letter");

        HashMap<String, Object> map = objectMapper.convertValue(
                new CianRefundRequest(id, cause), new TypeReference<>() {
                }
        );
        var req = new HttpEntity<>(map, refundHeaders);

        return restTemplate.postForObject(
                        "https://ud-api.cian.ru/feedback-form/v1/send-feedback/", req, CianRefundResponse.class)
                .getTicketId();
    }
}
