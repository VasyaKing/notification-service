package com.ebatkontora.notificationservice.service.impl;

import com.ebatkontora.notificationservice.dto.AmoContactsRequest;
import com.ebatkontora.notificationservice.dto.AmoLeadPatchRequest;
import com.ebatkontora.notificationservice.dto.AmoLeadRequest;
import com.ebatkontora.notificationservice.dto.AmoLeadsResponse;
import com.ebatkontora.notificationservice.dto.AmoLeadsResponse.Lead;
import com.ebatkontora.notificationservice.dto.AmoLinkRequest;
import com.ebatkontora.notificationservice.dto.AmoTokenResponse;
import com.ebatkontora.notificationservice.dto.Notification;
import com.ebatkontora.notificationservice.dto.Tag;
import com.ebatkontora.notificationservice.service.AmoFeignClient;
import com.ebatkontora.notificationservice.service.AmoService;
import com.ebatkontora.notificationservice.service.CianService;
import com.ebatkontora.notificationservice.store.LocalStore;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.grizzly.utils.Pair;
import org.redisson.api.RQueue;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AmoServiceImpl implements AmoService {

    private static final int REFUND_STATUS_ID = 42952714;
    private static final int REFUND_PIPELINE_ID = 4689295;
    private static final int REFUND_IN_PROGRESS_STATUS_ID = 42952717;
    private static final int REFUND_CAUSE_FIELD_ID = 659731;
    private static final int CIAN_NUMBER_FIELD_ID = 655389;
    private static final int CIAN_TAG_ID = 310751;
    private static final String CREATE_LEAD_QUEUE_NAME = "createLeadQueue";
    private static final HttpHeaders HTTP_HEADERS = new HttpHeaders();
    private static final Map<String, String> REFUND_MESSAGE_MAP = Map.of(
            "492603", "есть свой агент",
            "492605", "не выходит на связь",
            "492607", "оставлена ошибочно",
            "492609", "продают / покупают сами",
            "493359", "дубль"
    );
    private final LocalStore localStore;
    private final RestTemplate restTemplate;
    private final AmoFeignClient amoFeignClient;
    private final CianService cianService;
    private final Logger log = LogManager.getLogger();
    private final RQueue<Pair<Notification, List<AmoLeadRequest>>> createLeadQueue;
    private final RedisTemplate<String, String> amoTokenRedisTemplate;

    @Value("${amo.integrationId}")
    private String integrationId;

    @Value("${amo.redirect_uri}")
    private String redirectUri;

    @Value("${amo.uri}")
    private String amoUri;

    @Value("${cian.base-url}")
    private String cianBaseUrl;

    public AmoServiceImpl(LocalStore localStore,
                          RestTemplate restTemplate,
                          AmoFeignClient amoFeignClient,
                          CianService cianService,
                          RedisTemplate<String, String> amoTokenRedisTemplate,
                          RedissonClient redissonClient,
                          @Value("${amo.secretKey}") String secretKey
    ) {
        this.localStore = localStore;
        this.restTemplate = restTemplate;
        this.cianService = cianService;
        this.amoFeignClient = amoFeignClient;
        this.amoTokenRedisTemplate = amoTokenRedisTemplate;
        this.localStore.getCredentials().put("secretKey", secretKey);
        this.createLeadQueue = redissonClient.getQueue(CREATE_LEAD_QUEUE_NAME);
    }

    @Override
    public String createLead(Notification n) {
        log.info("Start creating lead from notification {}", n.getId());

        var customFields = List.of(
                AmoLeadRequest.CustomField.builder()
                        .fieldId(655389) //Номер заявки
                        .value(new AmoLeadRequest.Value(n.getId()))
                        .build(),
                AmoLeadRequest.CustomField.builder()
                        .fieldId(656793) //Расположение
                        .value(new AmoLeadRequest.Value(getLocation(n)))
                        .build(),
                AmoLeadRequest.CustomField.builder()
                        .fieldId(655403) //Бюджет запрос
                        .value(new AmoLeadRequest.Value(n.getFormattedPrice()))
                        .build(),
                AmoLeadRequest.CustomField.builder()
                        .fieldId(657031) //Тип клиента и сделки
                        .value(new AmoLeadRequest.Value(getTags(n)))
                        .build(),
                AmoLeadRequest.CustomField.builder()
                        .fieldId(656791) //Комментарий
                        .value(new AmoLeadRequest.Value(n.getComment()))
                        .build(),
                AmoLeadRequest.CustomField.builder()
                        .fieldId(655411) //Ссылка
                        .value(new AmoLeadRequest.Value(cianBaseUrl + n.getId()))
                        .build(),
                AmoLeadRequest.CustomField.builder()
                        .fieldId(657033) //Запрос
                        .value(new AmoLeadRequest.Value(getRequest(n)))
                        .build(),
                AmoLeadRequest.CustomField.builder()
                        .fieldId(655409) //Дата заявки
                        .value(new AmoLeadRequest.Value(n.getStartDate()))
                        .build(),
                AmoLeadRequest.CustomField.builder()
                        .fieldId(657035) //Параметры
                        .value(new AmoLeadRequest.Value(n.getParams()))
                        .build(),
                AmoLeadRequest.CustomField.builder()
                        .fieldId(655395) //Телефон подменный
                        .value(new AmoLeadRequest.Value(n.getCustomerTelephone()))
                        .build()
        );

        var request = Collections.singletonList(
                AmoLeadRequest.builder()
                        .name("")
                        .embedded(new AmoLeadRequest.Embedded(Collections.singletonList(new AmoLeadRequest.Tag(310751))))
                        .pipelineId(4619221)
                        .customFieldsValues(customFields)
                        .build()
        );

        return saveLeadWithContact(request, n);
    }

    @Override
    public void updateTokens() {
        log.info("Try to update access token");
        String secretKey = localStore.getCredentials().get("secretKey");
        String refreshToken = amoTokenRedisTemplate.opsForValue().get("refreshToken");

        if (secretKey == null || refreshToken == null) {
            log.error("secretKey or refreshToken is null, can't update token");
            throw new IllegalStateException("secretKey or refreshToken is null, can't update token");
        }

        Map<String, String> body = Map.of(
                "client_id", integrationId,
                "client_secret", secretKey,
                "refresh_token", refreshToken,
                "grant_type", "refresh_token",
                "redirect_uri", redirectUri
        );

        var response = restTemplate.postForEntity(
                amoUri + "/oauth2/access_token", body, AmoTokenResponse.class);
        updateStore(response);

        log.info("Access token was updated");
    }

    @Override
    public void setCode(String code) {
        localStore.getCredentials().put("code", code);
        log.info("code is updated");
    }

    @Override
    public void init() {
        initTokens();
    }

    @Override
    public Map<String, Pair<String, String>> refundProcess() {
        var response = new HashMap<String, Pair<String, String>>();
        var fiveDaysAgo = LocalDateTime.now().minusDays(7);
        Optional<AmoLeadsResponse> leads = amoFeignClient
                .getLeads(HTTP_HEADERS, REFUND_PIPELINE_ID, REFUND_STATUS_ID, 1, 50);
        if (leads.isEmpty()) {
            log.info("There is empty refund list");
            return Map.of();
        }
        List<Lead> newLeads = leads.get().getEmbedded().getLeads();

        newLeads.stream()
                .filter(lead -> REFUND_STATUS_ID == lead.getStatusId()
                        && isCianRefund(lead)
                        && lead.getCreateDate().isAfter(fiveDaysAgo))
                .forEach(lead ->
                {
                    String info = "";
                    String cianId;
                    try {
                        cianId = getCianIdFromAmoLead(lead);
                        var causeText = getRefundCauseText(lead);
                        try {
                            info = cianService.sendRefundLetter(cianId, causeText);
                            log.info("Sent refund letter for cianId: {}, amo leadId: {}", cianId, lead.getId());

                            try {
//                                amoFeignClient.updateLeads(localStore.getAmoHeaders(), lead.getId(),
//                                        new AmoLeadPatchRequest(REFUND_IN_PROGRESS_STATUS_ID)); //todo seems this config doesn't work (INVALID PATCH)

                                var request = new HttpEntity<>(new AmoLeadPatchRequest(REFUND_IN_PROGRESS_STATUS_ID), HTTP_HEADERS); //todo change on feign
                                restTemplate.patchForObject(amoUri + "/api/v4/leads/" + lead.getId(), request, Object.class);

                                log.info("Updated status for amo leadId: {}", lead.getId());
                            } catch (Exception e) {
                                log.error("Can't update status for leadId: {}", lead.getId());
                                log.error(e.getMessage());
                            }
                        } catch (Exception e) {
                            log.error("Can't send refund letter to CIAN. leadId: {}", lead.getId());
                            log.error(e.getMessage());
                            info = e.getMessage();
                        }
                    } catch (Exception e) {
                        log.error(e.getMessage());
                        cianId = "unknown";
                    }
                    response.put(lead.getId(), new Pair<>(cianId, info));

                });


        return response;
    }

    private String saveLeadWithContact(List<AmoLeadRequest> request, Notification n) {
        try {
            return saveLead(request, n);
        } catch (HttpClientErrorException e) {
            log.error("Error while saving leads to amo. {}", e.getMessage());

            if (HttpStatus.UNAUTHORIZED == e.getStatusCode()) {
                log.error("UNAUTHORIZED create lead request to AMO");
                createLeadQueue.add(new Pair<>(n, request));
            }
            return "UNKNOWN";
        }
    }

    private String saveLead(List<AmoLeadRequest> request, Notification n) {
        var savedLeads = amoFeignClient.saveLeads(HTTP_HEADERS, request);
        String id = savedLeads.getEmbedded().getLeads().get(0).getId();
        createAndLinkContactToLead(id, n);
        return id;
    }

    private void createAndLinkContactToLead(String savedLeadId, Notification n) {
        log.info("Create contact for saved lead with id = {}", savedLeadId);
        var createContactRequest = AmoContactsRequest.builder()
                .name("Добавить имя")
                .customFieldsValues(
                        List.of(
                                AmoContactsRequest.CustomField.builder()
                                        .fieldId(111067) //телефон
                                        .value(new AmoContactsRequest.Value(n.getCustomerTelephone(), 159591)) //рабочий
                                        .build()
                        )
                )
                .build();

        var savedContacts = amoFeignClient.saveContacts(HTTP_HEADERS, List.of(createContactRequest));
        var contactId = savedContacts.getEmbedded().getContacts().get(0).getId();
        var linkRequest = AmoLinkRequest.builder()
                .toEntityId(savedContacts.getEmbedded().getContacts().get(0).getId())
                .toEntityType("contacts")
                .build();

        amoFeignClient.linkToLead(HTTP_HEADERS, savedLeadId, List.of(linkRequest));
        log.info("Linked contact with id = {} for saved lead with id = {}", contactId, savedLeadId);
    }

    private boolean isCianRefund(Lead lead) {
        return Objects.nonNull(lead.getNestedWrapper())
                && !CollectionUtils.isEmpty(lead.getNestedWrapper().getTags())
                && lead.getNestedWrapper().getTags().stream()
                .anyMatch(n -> CIAN_TAG_ID == n.getId());
    }

    private String getRefundCauseText(Lead lead) {
        return lead.getCustomFieldsValues().stream()
                .filter(f -> REFUND_CAUSE_FIELD_ID == f.getId())
                .map(f -> REFUND_MESSAGE_MAP.get(f.getValues().get(0).getId()))
                .filter(Objects::nonNull)
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Can't get refund cause for lead: " + lead));
    }

    private String getCianIdFromAmoLead(Lead lead) {
        return lead.getCustomFieldsValues().stream()
                .filter(customField -> CIAN_NUMBER_FIELD_ID == customField.getId())
                .flatMap(customField -> customField.getValues().stream())
                .map(AmoLeadsResponse.Value::getValue)
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Can't get CIAN id from lead: " + lead));
    }

    private void initTokens() {
        log.info("Init token request");
        String secretKey = localStore.getCredentials().get("secretKey");
        String code = localStore.getCredentials().get("code");

        if (secretKey == null || code == null) {
            log.error("secretKey = {}  and code = {} mustn't be null",
                    secretKey, code);
            throw new IllegalStateException("secretKey and code mustn't be null");
        }

        Map<String, String> body = Map.of(
                "client_id", integrationId,
                "client_secret", secretKey,
                "code", code,
                "grant_type", "authorization_code",
                "redirect_uri", redirectUri
        );

        var response = restTemplate.postForEntity(
                amoUri + "/oauth2/access_token", body, AmoTokenResponse.class);
        updateStore(response); //todo change on feign
    }

    private void updateStore(ResponseEntity<AmoTokenResponse> response) {
        log.info("Amo response status: {}", response.getStatusCode());

        if (response.getStatusCode().is2xxSuccessful()) {
            if (response.getBody() == null) {
                throw new IllegalStateException("Amo response is empty");
            }
            var accessToken = response.getBody().getAccessToken();
            var refreshToken = response.getBody().getRefreshToken();
            amoTokenRedisTemplate.opsForValue().set("accessToken", accessToken);
            amoTokenRedisTemplate.opsForValue().set("refreshToken", refreshToken);
            log.info("Tokens in store was updated");

            initAmoHeaders();
        }
    }

    public void initAmoHeaders() {
        HTTP_HEADERS.setContentType(MediaType.APPLICATION_JSON);
        HTTP_HEADERS.setBearerAuth(Objects.requireNonNull(amoTokenRedisTemplate.opsForValue().get("accessToken")));
        log.info("Headers refreshed");
    }

    @Override
    public void recreateLeads() {
        if (!createLeadQueue.isEmpty()) {
            Pair<Notification, List<AmoLeadRequest>> pair = createLeadQueue.poll();
            log.info("Recreating amo lead: {}", pair.getSecond());
            saveLeadWithContact(pair.getSecond(), pair.getFirst());
        }
    }

    private String getTags(Notification n) {
        return n.getTags().stream().map(Tag::getName).collect(Collectors.joining(", "));
    }

    private String getLocation(Notification n) {
        return n.getGeoMainText() + " / " + n.getUndergrounds().stream().map(Tag::getName).collect(Collectors.joining(", "));
    }

    private String getRequest(Notification n) {
        return n.getDealTypeText() + " " + n.getRoomsCountText() + " " + n.getAreaText();
    }

}
