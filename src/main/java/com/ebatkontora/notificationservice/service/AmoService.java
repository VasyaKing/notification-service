package com.ebatkontora.notificationservice.service;

import com.ebatkontora.notificationservice.dto.Notification;
import org.glassfish.grizzly.utils.Pair;

import java.util.Map;

public interface AmoService {
    String createLead(Notification n);

    void setCode(String code);

    void init();

    Map<String, Pair<String, String>> refundProcess();

    void initAmoHeaders();

    void recreateLeads();

    void updateTokens();
}
