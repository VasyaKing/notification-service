package com.ebatkontora.notificationservice.service;

import com.ebatkontora.notificationservice.dto.CianBuyResponse;
import com.ebatkontora.notificationservice.dto.Notification;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.web.client.UnknownContentTypeException;

import java.util.List;

public interface CianService {
    List<Notification> pollNotifications() throws UnknownContentTypeException;

    void filterAndBuy(Notification n);

    CianBuyResponse.MessageData getMessageDataByNotificationById(String id) throws JsonProcessingException;

    void buyNotification(Notification notification);

    String sendRefundLetter(String id, String cause);

    void init();
}
