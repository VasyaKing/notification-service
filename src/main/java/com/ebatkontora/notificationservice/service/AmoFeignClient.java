package com.ebatkontora.notificationservice.service;

import com.ebatkontora.notificationservice.config.FeignConfiguration;
import com.ebatkontora.notificationservice.dto.AmoContactsRequest;
import com.ebatkontora.notificationservice.dto.AmoContactsResponse;
import com.ebatkontora.notificationservice.dto.AmoLeadPatchRequest;
import com.ebatkontora.notificationservice.dto.AmoLeadRequest;
import com.ebatkontora.notificationservice.dto.AmoLeadsResponse;
import com.ebatkontora.notificationservice.dto.AmoLinkRequest;
import com.ebatkontora.notificationservice.dto.AmoPipelinesResponse;
import com.ebatkontora.notificationservice.dto.AmoUsersResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@FeignClient(name = "amoClient", url = "${amo.uri}")
public interface AmoFeignClient
{
    @GetMapping("/api/v4/users")
    AmoUsersResponse getUsers(@RequestHeader HttpHeaders httpHeaders);

    @GetMapping("/api/v4/leads")
    Optional<AmoLeadsResponse> getLeads(@RequestHeader HttpHeaders httpHeaders,
                                       @RequestParam(name = "filter[statuses][0][pipeline_id]") int pipelineId,
                                       @RequestParam(name = "filter[statuses][0][status_id]") int statusId,
                                       @RequestParam(required = false) int page,
                                       @RequestParam(required = false) int limit);

    @GetMapping("/api/v4/leads/pipelines")
    AmoPipelinesResponse getPipelines(@RequestHeader HttpHeaders httpHeaders);

    @PatchMapping("/api/v4/leads/{id}")
    void updateLeads(@RequestHeader HttpHeaders httpHeaders,
                     @PathVariable String id,
                     @RequestBody AmoLeadPatchRequest request);

    @PostMapping("/api/v4/leads")
    AmoLeadsResponse saveLeads(@RequestHeader HttpHeaders httpHeaders,
                               @RequestBody List<AmoLeadRequest> request);

    @PostMapping("/api/v4/contacts")
    AmoContactsResponse saveContacts(@RequestHeader HttpHeaders httpHeaders,
                                     @RequestBody List<AmoContactsRequest> request);

    @PostMapping("/api/v4/leads/{entityId}/link")
    void linkToLead(@RequestHeader HttpHeaders httpHeaders,
                      @PathVariable String entityId,
                      @RequestBody List<AmoLinkRequest> request);

}
