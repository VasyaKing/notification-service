package com.ebatkontora.notificationservice.service;

import com.ebatkontora.notificationservice.dto.CianBuyResponse;
import com.ebatkontora.notificationservice.dto.CianError;
import com.ebatkontora.notificationservice.dto.Notification;
import com.ebatkontora.notificationservice.store.ActionCondition;
import com.ebatkontora.notificationservice.store.LocalStore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import javax.annotation.PostConstruct;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static com.ebatkontora.notificationservice.Utils.waitSomeTime;

@Component
public class TelegramBot extends TelegramLongPollingBot {

    private final LocalStore store;
    private final CianService cianService;
    private final AmoService amoService;
    private final ExecutorService executorService;
    private final AtomicBoolean isSendingActive = new AtomicBoolean(false);
    private final AtomicBoolean isRefundActive = new AtomicBoolean(false);
    private final Logger log = LogManager.getLogger();

    private final ObjectMapper objectMapper;
    private final RedisTemplate<String, Notification> notificationRedisTemplate;

    @Value("${bot.name}")
    private String botUsername;

    @Value("${cian.base-url}")
    private String cianBaseUrl;

    @Value("${bot.token}")
    private String botToken;

    @Value("${secret}")
    private String secret;

    @Value("${version}")
    private String version;

    @Value("${cian.timeout}")
    private int timeout;

    @Autowired
    public TelegramBot(LocalStore store, CianService cianService, AmoService amoService, ObjectMapper objectMapper,
                       RedisTemplate<String, Notification> redisTemplate) {
        this.store = store;
        this.cianService = cianService;
        this.amoService = amoService;
        this.objectMapper = objectMapper;
        this.notificationRedisTemplate = redisTemplate;
        executorService = Executors.newSingleThreadExecutor();
    }

    @PostConstruct
    public void inform() {
        createAndSendMessage("BOT RELOADED, version " + version, "101006488");
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {
            processMessage(update.getMessage());
        }
    }

    public String getBotUsername() {
        return botUsername;
    }

    public String getBotToken() {
        return botToken;
    }

    //todo refactor with event usage
    //todo run commands in other thread
    private void processMessage(Message message) { //todo add turn off/on amo notifications
        if (Objects.nonNull(message.getReplyToMessage())) {
            processReplyMessage(message);
            return;
        }

        String text = message.getText();
        String chatId = String.valueOf(message.getChatId());

        log.info("MESSAGE. chatId: {}", chatId);
        log.info(text);
        log.info("---------");

        if (text.equals(secret)) {
            registerChatId(chatId);
            return;
        }

        //todo do it more gracefully
        if (text.startsWith("amo")) {
            if (text.startsWith("amo code ")) {
                amoService.setCode(text.substring(9));
            } else if (text.startsWith("amo init")) {
                amoService.init();
            } else {
                log.error("Unknown amo command");
            }
            return;
        }

        if (text.startsWith("cian cookie ")) {
            store.getCredentials().put("cianCookie", text.substring(12));
            cianService.init();
            return;
        }

        switch (text) {
            case "help": {
                sendHelp(chatId);
                break;
            }
            case "подписка": {
                registerChatId(chatId);
                break;
            }
            case "отписка": {
                unregisterChatId(chatId);
                break;
            }
            case "старт": {
                startSendingNotifications(true);
                break;
            }
            case "стоп": {
                stopSendingNotifications(false);
                break;
            }
            case "старт покупка": {
                store.getConditions().put(ActionCondition.BUY, true);
                break;
            }
            case "старт продажа": {
                store.getConditions().put(ActionCondition.SELL, true);
                break;
            }
            case "стоп покупка": {
                store.getConditions().put(ActionCondition.BUY, false);
                break;
            }
            case "стоп продажа": {
                store.getConditions().put(ActionCondition.SELL, false);
                break;
            }
            case "статус": {
                sendStatus(chatId);
                break;
            }
            case "старт возврат": {
                isRefundActive.set(true);
                processRefundLeads();
                break;
            }
            case "стоп возврат": {
                isRefundActive.set(false);
                break;
            }
        }
    }

    private void processReplyMessage(Message message) {
        String replyMessageText = message.getReplyToMessage().getText();
        log.info("Start process reply message from chatId: {}", message.getChatId());
//        String notificationId = replyMessageText.substring(0, replyMessageText.indexOf(' '));
//        Notification notification = Optional.ofNullable(notificationRedisTemplate.opsForValue().get(notificationId))
//                .orElseThrow(() -> new IllegalStateException("There is no notification with id: " + notificationId));
        Notification notification = new Notification();
        String notificationId = "2064429";
        log.info("Notification from redis = {}", notification);
        try {
            CianBuyResponse.MessageData messageData = cianService.getMessageDataByNotificationById(notificationId);
            notification.setComment(messageData.getComment());
            notification.setParams(messageData.getParams());
            notification.setCustomerName(messageData.getFullName());
            notification.setCustomerTelephone(messageData.getPhoneNumber());
//            amoService.createLead(notification);
            log.info("Finish process reply message from chatId: {}", message.getChatId());
        } catch (JsonProcessingException e) {
            log.error("Can't process notification by id = {} from cian", notificationId);
            throw new RuntimeException(e);
        }

    }

    private void sendHelp(String chatId) {
        var text = new StringJoiner("\n")
                .add("Список возможных команд:")
                .add("подписка - для получения оповещений от амо")
                .add("отписка - для отключения оповещений от амо")
                .add("старт - включение получения заявок от циан")
                .add("стоп - отключение получения заявок от циан")
                .add("старт покупка - покупка заявок категории \"Купить\"")
                .add("старт продажа - покупка заявок категории \"Продажа\"")
                .add("стоп покупка - остановка покупки заявок категории \"Купить\"")
                .add("стоп продажа - остановка покупки заявок категории \"Продажа\"")
                .add("статус - информация о текущих условиях обработки заявок")
                .add("старт возврат - запуск отправки писем на возврат в циан")
                .add("стоп возврат - остановка отправки писем на возврат в циан")
                .toString();

        createAndSendMessage(text, chatId);
    }

    private void sendStatus(String chatId) {
        try {
            var notifications = cianService.pollNotifications();
            var text = new StringJoiner("\n")
                    .add("Версия = " + version)
                    .add("Рассылка  = " + isSendingActive.get())
                    .add("Возврат  = " + isRefundActive.get())
                    .add("Покупка  = " + store.getConditions().get(ActionCondition.BUY))
                    .add("Продажа  = " + store.getConditions().get(ActionCondition.SELL))
                    .add("Количество заявок = " + notifications.size())
                    .add("Количество чатов = " +
                            store.getChatIdsForCian().values().stream().mapToInt(v -> v ? 1 : 0).sum())
                    .toString();
            createAndSendMessage(text, chatId);
        } catch (HttpServerErrorException e) {
            log.error("Can't send status");
            String message = e.getMessage();
            if (message != null) { //todo refactor me pls
                int maxLogSize = 1000;
                for (int i = 0; i <= message.length() / maxLogSize; i++) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = Math.min(end, message.length());
                    log.error(message.substring(start, end));
                }
            }
        }
    }

    private void registerChatId(String chatId) {
        store.getChatIdsForCian().put(chatId, true);
        log.info("Added chatId = {}", chatId);
        createAndSendMessage("Вы зарегистрировались. Бот начнёт присылать вам оповещения", chatId);
    }

    private void unregisterChatId(String chatId) {
        var isRemoved = store.getChatIdsForCian().putIfAbsent(chatId, false);
        if (Objects.nonNull(isRemoved) && isRemoved) {
            log.info("Removed chatId = {}", chatId);
            createAndSendMessage("Вы отписались. Бот перестанет присылать вам оповещения", chatId);
        } else {
            log.info("Can't remove chatId = {}", chatId);
            createAndSendMessage("Что-то пошло не так, не получилось отписаться от рассылки", chatId);

        }
    }

    public void startSendingNotifications(Boolean needToSend) {
        isSendingActive.set(true);
        if (needToSend) {
            sendMessageInAllChats("Рассылка включена");
        }

        log.info("Sending start. isSendingActive = {} ", isSendingActive.get());
        store.resetNotificationStore();
        cianService.pollNotifications().forEach(n -> store.getNotifications().putIfAbsent(n.getId(), n));
        log.info("Warming store. notification count = {}", store.getNotifications().size());

        executorService.submit(() -> {
            while (isSendingActive.get()) {
                try {
                    var notifications = cianService.pollNotifications();
                    var newNotifications = findNewNotifications(notifications);
                    int size = newNotifications.size();
                    if (size > 0) {
                        log.info("Found {} new notifications", size);
                    }
                    newNotifications.forEach(this::processNotification);
                    waitSomeTime(timeout);
                } catch (Exception e) {
                    log.error("Can't poll notifications, message: {}", e.getMessage().substring(0, 20));
                    if (e.getMessage().contains("Forbidden")) {
                        isSendingActive.set(false);
                        sendMessageInAllChats("Получение заявок выключено:" + e.getMessage());
                    }
                    waitSomeTime(timeout * 10);
                }
            }
        });
    }

    public void stopSendingNotifications(Boolean needToSend) {
        isSendingActive.set(false);
        log.info("Sending stop. isSendingActive = {} ", isSendingActive.get());
        if (needToSend) {
            log.info(LocalTime.now());
            sendMessageInAllChats("Рассылка отключена");
        }
    }

    private List<Notification> findNewNotifications(List<Notification> notifications) {
        return notifications.stream()
                .filter(n -> Objects.isNull(store.getNotifications().get(n.getId())))
                .collect(Collectors.toList());
    }

    private void processNotification(Notification notification) {
        log.info("Process. Notification: {}", notification);
        store.getNotifications().putIfAbsent(notification.getId(), notification);

        cianService.filterAndBuy(notification);

        String amoLeadId = null;
        if (notification.isBought()) {
            amoLeadId = amoService.createLead(notification);
        }
        notificationRedisTemplate.opsForValue().set(notification.getId(), notification);
        sendMessageInAllChats(createText(notification, amoLeadId));
    }

    private void sendMessageInAllChats(String messageText) {
        store.getChatIdsForCian().entrySet().stream()
                .filter(Map.Entry::getValue)
                .forEach(e -> createAndSendMessage(messageText, e.getKey()));
    }

    private void createAndSendMessage(String messageText, String chatId) {
        SendMessage response = new SendMessage();
        response.setChatId(String.valueOf(chatId));
        response.setText(messageText);
        try {
            execute(response);
            log.info("ChatId: {}, Message send: {}", chatId, messageText);
        } catch (TelegramApiException e) {
            log.error("Failed send notification to {} due to error: {}", chatId, e.getMessage());
        }
    }

    private String createText(Notification n, String amoLeadId) {
        StringBuilder sb = new StringBuilder();
        if (n.isBought()) {
            sb.append("LEAD ");
        }
        sb.append(n.getId()).append(" ");
        sb.append(n.getOfferTypeText()).append(" ");
        sb.append(n.getDealTypeText()).append(" ");
        sb.append(n.getRoomsCountText()).append(" ");
        sb.append(n.getAreaText()).append(" ");
        sb.append(n.getFormattedPrice()).append("\n");

        sb.append(n.getGeoMainText()).append(" / ");
        n.getUndergrounds().forEach(u -> sb.append(u.getName()).append(" / "));
        n.getTags().forEach(t -> sb.append(t.getName()).append(" / "));

        sb.append(cianBaseUrl).append(n.getId());

        if (Objects.nonNull(amoLeadId)) {
            sb.append(String.format("https://agentexpert.amocrm.ru/leads/detail/%s", amoLeadId));
        }

        if (!Objects.isNull(n.getErrorMessage())) {
            String text;
            try {
                TypeReference<List<CianError>> typeRef = new TypeReference<>() {};
                CianError cianError = objectMapper.readValue(n.getErrorMessage(), typeRef).get(0);
                text = cianError.getMessage();
            } catch (Exception e) {
                log.error(e.getStackTrace());
                log.error(e.getMessage());
                text = n.getErrorMessage();
            }
            sb.append("\n");
            sb.append("Ошибка при покупке: ");
            sb.append(text);
        }
        return sb.toString();
    }

    public void relogin() { //todo investigate - may be remove it
        if (isSendingActive.get()) {
            log.info("Relogin init");
            stopSendingNotifications(false);
            List<Notification> notifications = null;
            while (Objects.isNull(notifications)) {
                waitSomeTime(timeout * 3);
                try {
                    notifications = cianService.pollNotifications();
                    startSendingNotifications(false);
                    log.info("Relogin finished");
                } catch (Exception e) {
                    log.error("Can't relogin, message: {}", e.getMessage());
                    waitSomeTime(timeout * 10);
                }
            }
        }
    }

    public void healthCheck() {
        if (isSendingActive.get()) {
            var notifications = cianService.pollNotifications();
            log.info("Health check job. There are {} notifications in list", notifications.size());
        }
    }

    public void processRefundLeads() {
        if (isRefundActive.get()) {
            log.info("Start refund process");
            var leadsIdMap = amoService.refundProcess();
            leadsIdMap.forEach((amoId, value) -> {
                var cianId = value.getFirst();
                var ticketId = value.getSecond();
                var text = String.format("Возврат. Номер обращения %s\n" +
                        "amo: https://agentexpert.amocrm.ru/leads/detail/%s\n" +
                        "cian: https://my.cian.ru/leads/%s", ticketId, amoId, cianId);
                createAndSendMessage(text, "384674648"); //aziz
                createAndSendMessage(text, "101006488"); //me
            });
            log.info("Finish refund process");
        } else {
            log.info("Can't start refund process, isRefundActive = {}", isRefundActive.get());
        }
    }

}
