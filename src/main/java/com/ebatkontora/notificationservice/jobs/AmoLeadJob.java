package com.ebatkontora.notificationservice.jobs;

import com.ebatkontora.notificationservice.service.AmoService;
import com.ebatkontora.notificationservice.service.TelegramBot;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class AmoLeadJob {
    private final TelegramBot telegramBot;
    private final AmoService amoService;

    @Scheduled(cron = "${scheduler.refund-amo-lead}")
    public void refundLeads() {
        telegramBot.processRefundLeads();
    }

    @Scheduled(cron = "${scheduler.recreate-amo-lead}")
    public void retryLeadCreation() {
        amoService.recreateLeads();
    }

    @Scheduled(cron = "${scheduler.update-amo-tokens}")
    public void updateTokens() {
        amoService.updateTokens();
    }
}
