package com.ebatkontora.notificationservice.jobs;

import com.ebatkontora.notificationservice.service.TelegramBot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ReloginJob {

    private final TelegramBot telegramBot;

    @Autowired
    public ReloginJob(TelegramBot telegramBot) {
        this.telegramBot = telegramBot;
    }

    @Scheduled(cron = "${scheduler.relogin}")
    public void relogin() {
        telegramBot.relogin();
    }

    @Scheduled(cron = "${scheduler.health-check}")
    public void healthCheck() {
        telegramBot.healthCheck();
    }

    @Scheduled(cron = "${scheduler.start}")
    public void start() {
        telegramBot.startSendingNotifications(true);
    }

    @Scheduled(cron = "${scheduler.stop}")
    public void stop() {
        telegramBot.stopSendingNotifications(true);
    }
}
